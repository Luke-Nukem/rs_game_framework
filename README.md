# rs_game_framework

This is a work-in-progress which I am using to learn some of the finer details
of Rust. Eventually I hope to make it reasonably easy to use for various
simplistic games, eg Tetris, Pacman, Space Invaders.

### Goals

* Provide a simple framework which is easy to build a new set of Game and Menu
  states in to.
* Turn it in to a module (this may be a bit harder to manage, depending on how
  the first bit goes)
* Document all the code for use with `rustdoc`
* Add unit tests that can check that all the required parts of the framework
  work correctly
* Learn!

### Wishlist

* GLSL shaders for the openGL renderer

Uses rust-sdl2 and toml crates.

[**MIT license**](LICENSE)
