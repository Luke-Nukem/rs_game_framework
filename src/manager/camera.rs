use crate::vec2d::Vec2d;
use sdl2::rect::Rect;

pub struct Camera {
    // Ratios
    to_screen_ratio: Vec2d<i32>,
    to_world_ratio: Vec2d<i32>,
    // Zoom
    zoom_extents: Rect,
    // Display size
    screen_extents: Rect,
    // World size
    world_extents: Rect,
}

impl Camera {
    pub fn new() -> Camera {
        Camera {
            to_screen_ratio: Vec2d::default(),
            to_world_ratio: Vec2d::default(),
            zoom_extents: Rect::new(0, 0, 0, 0),
            screen_extents: Rect::new(0, 0, 0, 0),
            world_extents: Rect::new(0, 0, 0, 0),
        }
    }

    pub fn set(
        &mut self, screen_extents: Rect, world_extents: Rect,
        screen_extents_in_world: Rect,
    ) {
        self.to_screen_ratio.set_x(
            (screen_extents.width() / screen_extents_in_world.width()) as i32,
        );
        self.to_screen_ratio.set_y(
            (screen_extents.height() / screen_extents_in_world.height()) as i32,
        );
        self.to_world_ratio.set_x(1 / self.to_screen_ratio.x());
        self.to_world_ratio.set_y(1 / self.to_screen_ratio.y());

        self.screen_extents = screen_extents;
        self.world_extents = world_extents;
        self.zoom_extents = screen_extents_in_world;
    }

    pub fn update_position(&mut self, position: Vec2d<i32>) {
        let mut new_x = position.x() - self.zoom_extents.width() as i32 / 2;
        let mut new_y = position.y() - self.zoom_extents.height() as i32 / 2;

        // Bounds
        if new_x < self.world_extents.left() {
            new_x = self.world_extents.left();
        } else if new_x + self.zoom_extents.width() as i32
            > self.world_extents.right()
        {
            new_x = self.world_extents.width() as i32
                - self.zoom_extents.width() as i32
        }

        if new_y < self.world_extents.y() {
            new_y = self.world_extents.y()
        } else if new_y + self.zoom_extents.height() as i32
            > self.world_extents.height() as i32
        {
            new_y = self.world_extents.height() as i32
                - self.zoom_extents.height() as i32
        }

        self.zoom_extents.set_x(new_x);
        self.zoom_extents.set_y(new_y);
    }

    pub fn world_to_screen(&self, world: Vec2d<i32>) -> Vec2d<i32> {
        let x = self.screen_extents.x()
            + self.to_screen_ratio.x()
                * (world.x() - self.world_extents.x() - self.zoom_extents.x());

        let y = self.screen_extents.y()
            + self.to_screen_ratio.y()
                * (world.y() - self.world_extents.y() - self.zoom_extents.y());

        Vec2d::new(x, y)
    }

    pub fn position(&self) -> &Rect {
        &self.zoom_extents
    }

    pub fn world_ratio(&self) -> &Vec2d<i32> {
        &self.to_screen_ratio
    }
}
