/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

use sdl2::rect::Rect;
use sdl2::render::Texture;
use std::collections::HashMap;

/// A store of sprite sheets for sprite selection
///
/// This is typically used for storing preloaded spritesheets
/// which can then have sections rendered by placing a Rect
/// at the required location in the sheet
pub struct SpriteSheets {
    store: HashMap<String, Texture>,
}
impl SpriteSheets {
    pub fn new() -> SpriteSheets {
        SpriteSheets {
            store: HashMap::new(),
        }
    }

    /// Insert a new sprite in to the `SpriteSheets`
    pub fn push(&mut self, id: &str, texture: Texture) {
        if !self.store.contains_key(id) {
            self.store.insert(id.to_string(), texture);
        } else {
            println!("{:?} exists", id);
        }
    }

    /// Return a ref to the `SpriteSheet` located under `SpriteId`
    pub fn get_ref(&self, id: &str) -> &Texture {
        match self.store.get(id) {
            Some(x) => x,
            None => panic!("Sprite ID does not exist"),
        }
    }
}

pub struct CachedTile {
    src_rect: Rect,
    src_name: String,
}

impl CachedTile {
    pub fn new(src_rect: Rect, src_name: String) -> Self {
        CachedTile { src_rect, src_name }
    }
    pub fn get_src_rect(&self) -> &Rect {
        &self.src_rect
    }
    pub fn get_src_name(&self) -> &str {
        &self.src_name
    }
}
