/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

use sdl2::{render::Canvas, video::Window};

use crate::{
    controls::{Bindings, Input},
    states::GameState,
    FP,
};

pub struct Pause {
    id: String,
}
impl Pause {
    pub fn new() -> Pause {
        Pause {
            id: "_PAUSE".to_string(),
        }
    }
}

impl GameState for Pause {
    fn update(&mut self, _dt: FP, _input: &Input, _bindings: &mut Bindings) {}
    fn render(&mut self, _dt: FP, canvas: &mut Canvas<Window>) {
        //let alive = Color::RGB(60, 1, 1);
        canvas.clear();
    }
    fn enter(&mut self) {}

    fn resume(&mut self) {}

    fn pause(&mut self) {}

    fn exit(&mut self) {}

    fn get_id(&self) -> &String {
        &self.id
    }
}
