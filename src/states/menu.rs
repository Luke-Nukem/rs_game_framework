/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

use sdl2::{render::Canvas, video::Window};

use crate::{
    controls::{Bindings, Input},
    states::GameState,
    FP,
};

pub struct Menu {
    id: String,
}
impl Menu {
    pub fn new() -> Menu {
        Menu {
            id: "_MENU".to_string(),
        }
    }
}

impl GameState for Menu {
    fn update(&mut self, _dt: FP, _input: &Input, _bindings: &mut Bindings) {}
    fn render(&mut self, _dt: FP, canvas: &mut Canvas<Window>) {
        //let alive = Color::RGB(200, 200, 230);
        canvas.clear();
    }
    fn enter(&mut self) {}

    fn resume(&mut self) {}

    fn pause(&mut self) {}

    fn exit(&mut self) {}

    fn get_id(&self) -> &String {
        &self.id
    }
}
