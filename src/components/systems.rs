/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

use std::collections::HashMap;

use crate::components::parts::*;
use crate::controls::Input;
use crate::messaging::MessageBus;
use crate::tiny_ecs::Entities;
use crate::FP;

/// Check for entities that have an `INPUT` and check if they had any input queued,
/// if so, add a message to the message queue keyed to that entity ID for processing
pub fn input_update(
    eid: usize,
    entities: &mut Entities,
    msg_bus: &mut MessageBus,
    input: &Input,
) {
    if entities.entity_contains::<InputTypes>(eid) {
        let mut input_map =
            entities.borrow_mut::<InputTypes>().expect("No input map");
        if let Some(input_type) = input_map.get_mut(eid) {
            //let input_type = entities.parts.input.get_mut(&eid).unwrap();
            // Is it PlayerInput?
            match input_type {
                InputTypes::Player(ref player) => {
                    for (key, message) in player.get_key_binds() {
                        if input.get_key(*key) {
                            msg_bus.insert(eid, *message);
                        }
                    }
                    for (mbtn, message) in player.get_mouse_binds() {
                        if input.get_mbtn(*mbtn) {
                            msg_bus.insert(eid, *message);
                        }
                    }
                }
                _ => {} // TODO: extra bindings for menu/inventory etc
            }
        }
    }
}

/// Update all animations
///
#[allow(unused_variables)]
pub fn animation_update(eid: usize, entities: &mut Entities, dt: FP) {}

/// Update all sprites
///
/// TODO: May or may not be used. Depends on if sprites are coming from a sheet and need a frame..
#[allow(unused_variables)]
pub fn sprite_update(
    active: &[usize],
    amask: &[u32],
    dt: FP,
    s: &mut HashMap<u32, Sprite>,
) {
}
