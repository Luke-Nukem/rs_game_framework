/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

use std::collections::HashMap;

use sdl2::rect::Rect;

use crate::controls::Bindings;
use crate::vec2d::Vec2d;

/// The most commonly used component. Not compulsory though
pub struct Position {
    pub pos: Vec2d<f64>,
    pub rot: f64,
}
impl Position {
    pub fn new(x: f64, y: f64, r: f64) -> Position {
        Position {
            pos: Vec2d::new(x, y),
            rot: r,
        }
    }
}

#[derive(Debug)]
pub struct ParticleID {
    pub id: usize,
}

pub struct Player {
    pub speed: f64,
    pub jump_h: f64,
}
impl Player {
    pub fn new(s: f64, jh: f64) -> Player {
        Player {
            speed: s,
            jump_h: jh,
        }
    }
}
impl Default for Player {
    fn default() -> Player {
        Player {
            speed: 3.0,
            jump_h: 15.0,
        }
    }
}

pub struct Ai {
    pub speed: f32,
    pub jump_h: f32,
}
impl Ai {
    pub fn new(s: f32, jh: f32) -> Ai {
        Ai {
            speed: s,
            jump_h: jh,
        }
    }
}
impl Default for Ai {
    fn default() -> Ai {
        Ai {
            speed: 7.0,
            jump_h: 20.0,
        }
    }
}

pub enum InputTypes {
    Player(Bindings),
    Menu(Bindings),
}

#[derive(Debug)]
pub struct Sprite {
    pub sid: String,
    pub frame: Rect,
}
impl Sprite {
    /// takes a ref as `spr` name to make other code easier
    pub fn new(spr: &str, frame: Rect) -> Sprite {
        Sprite {
            sid: spr.to_string(),
            frame,
        }
    }
}

/// links to a resource sheet and shifts frames over it
pub struct Animation {
    pub counter: u32,
    pub speed: u32,
    pub current: u32,
    pub start_frame: u32,
    pub per_anim: u32,
    pub columns: u32,
    /// Stores the name of the sequence and frame start number
    pub keys: HashMap<String, u32>,
    pub frame: Rect,
    pub sid: String,
}
impl Animation {
    pub fn new(
        x: i32, y: i32, w: u32, h: u32, speed: u32, per_anim: u32,
        columns: u32, keys: HashMap<String, u32>, sprite_id: String,
    ) -> Animation {
        Animation {
            counter: 0,
            speed,
            current: 0,
            start_frame: 0,
            per_anim,
            columns,
            keys,
            frame: Rect::new(x, y, w, h),
            sid: sprite_id,
        }
    }
    /// Used by the animation system to advance frames
    ///
    /// frames start at 0, eg; 5 frame anim - 0,1,2,3,4,0,1,2,3 etc
    pub fn next_frame(&mut self) {
        if self.counter == self.speed - 1 {
            self.current = (self.current + 1) % self.per_anim;
        }
        self.counter = (self.counter + 1) % self.speed;
    }

    pub fn draw_frame(&mut self) {
        let x = (self.current % self.columns) * self.frame.width();
        let y = (self.current / self.columns) * self.frame.height();
        self.frame.set_x(x as i32);
        self.frame.set_y(y as i32);
    }
    pub fn change_anim(&mut self, anim: &str) {
        match self.keys.get(anim) {
            Some(x) => {
                self.current = *x;
                self.start_frame = *x;
            }
            None => println!("Invalid animation key"), // TODO : logging
        }
    }
}

pub struct Health {
    pub current: i32,
    pub max: i32,
}
impl Health {
    pub fn new(current: i32, max: i32) -> Health {
        Health { current, max }
    }
}
impl Default for Health {
    fn default() -> Health {
        Health {
            current: 100,
            max: 200,
        }
    }
}

pub struct Armour {
    pub current: i32,
    pub max: i32,
}
impl Armour {
    pub fn new(current: i32, max: i32) -> Armour {
        Armour { current, max }
    }
}
impl Default for Armour {
    fn default() -> Armour {
        Armour {
            current: 100,
            max: 200,
        }
    }
}

pub struct Inventory {
    pub weapons: Vec<Weapon>,
    pub items: Vec<Item>,
}
impl Default for Inventory {
    fn default() -> Inventory {
        Inventory {
            weapons: Vec::new(),
            items: Vec::new(),
        }
    }
}

/// Any type of component that needs to be stored in one `HashMap` or array should use an enum to
/// generalise it.
pub enum Weapon {
    Ranged(WeaponRanged),
    Melee(WeaponMelee),
}

pub struct WeaponRanged {
    pub name: String,
    pub range: u32,
    pub damage: u32,
    pub speed: u32,
    pub spread: u32,
    pub max_ammo: u32,
    pub cur_ammo: u32,
    pub reload_speed: u32,
    pub shot_count: u32,
}
impl WeaponRanged {
    pub fn new(
        n: String, r: u32, d: u32, s: u32, sp: u32, ma: u32, ca: u32, rs: u32,
        sc: u32,
    ) -> WeaponRanged {
        WeaponRanged {
            name: n,
            range: r,
            damage: d,
            speed: s,
            spread: sp,
            max_ammo: ma,
            cur_ammo: ca,
            reload_speed: rs,
            shot_count: sc,
        }
    }
}
impl Default for WeaponRanged {
    fn default() -> WeaponRanged {
        WeaponRanged {
            name: "Pistol".to_string(),
            range: 100,
            damage: 5,
            speed: 100,
            spread: 1,
            max_ammo: 50,
            cur_ammo: 5,
            reload_speed: 2,
            shot_count: 1,
        }
    }
}

pub struct WeaponMelee {
    pub name: String,
    pub range: u32,
    pub damage: u32,
    pub speed: u32,
}
impl WeaponMelee {
    pub fn new(n: String, r: u32, d: u32, s: u32) -> WeaponMelee {
        WeaponMelee {
            name: n,
            range: r,
            damage: d,
            speed: s,
        }
    }
}
impl Default for WeaponMelee {
    fn default() -> WeaponMelee {
        WeaponMelee {
            name: "Fists".to_string(),
            range: 2,
            damage: 2,
            speed: 2,
        }
    }
}

// TODO: Enum of effects items can have? Ammo/Weapons...
/// Any type of component that needs to be stored in one `HashMap` or array should use an enum to
/// generalise it.
pub enum ItemEffects {
    Health(u32),
    Armour(u32),
}
pub struct Item {
    pub name: String,
    pub effect: ItemEffects,
}
impl Item {
    pub fn new(n: String, e: ItemEffects) -> Item {
        Item { name: n, effect: e }
    }
}
