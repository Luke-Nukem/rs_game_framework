/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

use sdl2::rect::Rect;

use crate::components::parts::*;
use crate::controls::Bindings;
use crate::manager::world::World;
use crate::messaging::PlayerListener;
use crate::particle2d::particle::Particle;
use crate::vec2d::Vec2d;
use tiny_ecs::ECSError;

/// Create a Player entity at the X,Y coords in the World
pub fn player(
    world: &mut World,
    x: u32,
    y: u32,
    sprite_id: &str,
) -> Result<usize, ECSError> {
    let mut player = Particle::new(1.0, 0.85);
    player.set_position(Vec2d::new(f64::from(x), f64::from(y)));
    player.set_acceleration(Vec2d::new(0.0, -9.81));
    let id = world.physics_world.add_particle(player).unwrap();
    let particle_ptr = world
        .physics_world
        .get_particle_ptr(id)
        .expect("Couldn't get player pointer");

    let rec = Rect::new(0, 0, 16, 16);
    // key bindings related to player actions
    let mut binds = Bindings::new();
    binds.init_player();

    let ent = world
        .entities
        .new_entity()
        .with(ParticleID { id })?
        .with(Sprite::new(sprite_id, rec))?
        .with(InputTypes::Player(binds))?
        .with(Player::new(5.0, 15.0))?
        .finalise()?;

    // Message listener to act on messages to player entity
    world.add_listener(PlayerListener::new(ent, particle_ptr));

    world.set_player(ent);
    Ok(ent)
}

/// Creates a background tile at X,Y in the World - requires a `sprite_id` and `Rect` for size,
/// the size of the sprite and rectangle don't need to match, the sprite will be drawn to the
/// size of the `Rect`.
pub fn background_tile(
    world: &mut World,
    x: u32,
    y: u32,
    sprite_id: &str,
    rec: Rect,
) -> Result<(), ECSError> {
    world
        .entities
        .new_entity()
        .with(Sprite::new(sprite_id, rec))?
        .finalise()?;
    Ok(())
}

/// Creates a foreground tile at X,Y in the World - requires a `sprite_id` and `Rect` for size,
/// the size of the sprite and rectangle don't need to match, the sprite will be drawn to the
/// size of the `Rect` and the `Rect` will also function as a collider AABB. The tile can
/// have physics properties
pub fn level_tile(
    world: &mut World,
    x: u32,
    y: u32,
    sprite_id: &str,
    rec: Rect,
) -> Result<(), ECSError> {
    world
        .entities
        .new_entity()
        .with(Sprite::new(sprite_id, rec))?
        .finalise()?;
    Ok(())
}

/// Creates an object at X,Y in the World - this is used for such things as interactive objects
/// like weapons, inventory items.
/// Requires a `sprite_id` and `Rect` for size, the size of the sprite and rectangle don't need
/// to match, the sprite will be drawn to the size of the `Rect` and the `Rect` will also
/// function as a collider AABB
pub fn object_tile(
    world: &mut World,
    x: u32,
    y: u32,
    sprite_id: &str,
    rec: Rect,
) -> Result<(), ECSError> {
    world
        .entities
        .new_entity()
        .with(Sprite::new(sprite_id, rec))?
        .finalise()?;
    Ok(())
}
