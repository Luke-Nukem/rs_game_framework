/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/// Methods for creating entities
///
/// Generally, these methods can conpose an entity from any parts
/// required and then push a mask in to the entity list which works
/// by the index number in the list being the entity ID for parts and
/// the mask containing the parts used.
pub mod create;

/// Parts that can be used to compose entities
///
/// Parts can (and should) be used with systems, and as such
/// they shouldn't have any methods attached to them - instead
/// a system will do the work required.
pub mod parts;

/// Systems that the World uses to work with entities
///
/// Systems can be used for, and do anything, as long as
/// it is only with entity parts - no other aspects of
/// the engine.
pub mod systems;
