/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

use crate::components::parts::*;
use crate::vec2d::Vec2d;
///
/// The main way of passing messages between components
///
use particle2d::particle::Particle;
use std::ptr::NonNull;
use tiny_ecs::Entities;
use vec_map::VecMap;

pub struct MessageBus {
    messages: VecMap<Vec<Message>>, // entity id, vec of all messages for that entity
}
impl MessageBus {
    pub fn new() -> MessageBus {
        MessageBus {
            messages: VecMap::new(),
        }
    }
    pub fn insert(&mut self, entity_id: usize, message: Message) {
        // See if we can get the existing vec for the entity_id
        if self.messages.contains_key(entity_id) {
            if let Some(entry) = self.messages.get_mut(entity_id) {
                entry.push(message)
            };
        } else {
            self.messages.insert(entity_id, vec![message]);
        }
    }
    /// Returns the vector of all messages for the entity_id, if any.
    pub fn get_msgs(&mut self, entity_id: usize) -> Option<&mut Vec<Message>> {
        self.messages.get_mut(entity_id)
    }
}

pub trait MessageListener {
    fn update(&mut self, msg_bus: &mut MessageBus, entities: &mut Entities) {
        if let Some(messages) = msg_bus.get_msgs(self.get_id()) {
            for msg in messages.drain(0..) {
                self.action(msg, entities)
            }
        }
    }
    fn action(&mut self, msg: Message, entities: &mut Entities);

    fn get_id(&self) -> usize;
}

pub struct PlayerListener {
    entity: usize,
    particle: NonNull<Particle>,
}

impl PlayerListener {
    pub fn new(entity: usize, particle: NonNull<Particle>) -> PlayerListener {
        PlayerListener { entity, particle }
    }
}

impl MessageListener for PlayerListener {
    fn get_id(&self) -> usize {
        self.entity
    }

    fn action(&mut self, msg: Message, entities: &mut Entities) {
        if let Ok(map) = Entities::borrow::<Player>(entities) {
            if let Some(player) = map.get(self.entity) {
                let mut action: Vec2d<f64> = Vec2d::default();
                match msg {
                    Message::MoveLeft => action.set_x(-player.speed),
                    Message::MoveUp => action.set_y(player.jump_h),
                    Message::MoveRight => action.set_x(player.speed),
                    Message::MoveDown => action.set_y(-player.speed),
                    Message::Shoot => {
                        println!("Shoot!");
                    }
                    Message::Jump => action.set_y(player.jump_h),
                    _ => {}
                };
                unsafe { self.particle.as_mut().add_force(action) };
            }
        }
    }
}

/// General purpose message structure
#[derive(Copy, Clone, Debug)]
pub enum Message {
    MoveLeft,
    MoveUp,
    MoveRight,
    MoveDown,
    Shoot,
    Jump,
    Explode,
}
